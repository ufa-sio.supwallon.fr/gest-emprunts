<?php
namespace MyProject\Controllers;

use MyProject\Kernel\Route;
use MyProject\Kernel\Router;
use MyProject\Kernel\View;
use MyProject\Model\Classes\Sample;

class HomeController
{
    public static function route()
    {
        // Routage secondaire
        $router = new Router();
        $router->addRoute(new Route("/", "MyProject\\Controllers\\HomeController", "homeAction"));

        $route = $router->findRoute();

        if ($route)
        {
            $route->execute();
        }
        else
        {
            // Erreur 404
            echo "Page not found";
        }
    }

    public static function homeAction()
    {
        $title = "My Project";

        View::setTemplate("home.tpl");
        View::bindVar("title", $title);
        View::display();
    }
}