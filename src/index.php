<?php
namespace MyProject;

require_once "vendor/autoload.php";

use MyProject\Kernel\Route;
use MyProject\Kernel\Router;

$router = new Router();
$router->addRoute(new Route("/", "MyProject\\Controllers\\HomeController"));

$route = $router->findRoute();

if ($route)
{
    $route->execute();
}
else
{
    // Erreur 404
    echo "Page not found";
}