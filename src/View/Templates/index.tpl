<!DOCTYPE html>
<html lang="en">
<?php require_once "head.tpl"; ?>
    <body>
        <main role="main" class="container">
<?php require_once $template; ?>
        </main>
<?php require_once "scripts.tpl"; ?>
    </body>
</html>