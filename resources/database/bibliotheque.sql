-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 10 fév. 2020 à 22:01
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bibliotheque`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
CREATE TABLE IF NOT EXISTS `adherent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `adherent`
--

INSERT INTO `adherent` (`id`, `nom`, `prenom`) VALUES
(1, 'DUPONT', 'Bernard'),
(2, 'DUPUIS', 'Bertrand'),
(3, 'DUFOUR', 'Brigitte');

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

DROP TABLE IF EXISTS `emprunt`;
CREATE TABLE IF NOT EXISTS `emprunt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debut` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cloture` datetime DEFAULT NULL,
  `id_adherent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_adherent` (`id_adherent`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `emprunt`
--

INSERT INTO `emprunt` (`id`, `debut`, `cloture`, `id_adherent`) VALUES
(1, '2020-02-10 10:08:42', '2020-02-11 17:31:16', 3),
(2, '2020-02-10 16:13:29', NULL, 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `emprunt_non_clos`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `emprunt_non_clos`;
CREATE TABLE IF NOT EXISTS `emprunt_non_clos` (
`id` int(11)
,`debut` datetime
,`cloture` datetime
,`id_adherent` int(11)
);

-- --------------------------------------------------------

--
-- Structure de la table `emprunt_ouvrage`
--

DROP TABLE IF EXISTS `emprunt_ouvrage`;
CREATE TABLE IF NOT EXISTS `emprunt_ouvrage` (
  `id_emprunt` int(11) NOT NULL,
  `id_ouvrage` int(11) NOT NULL,
  `rendu` datetime DEFAULT NULL,
  PRIMARY KEY (`id_emprunt`,`id_ouvrage`),
  KEY `id_emprunt` (`id_emprunt`),
  KEY `id_ouvrage` (`id_ouvrage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `emprunt_ouvrage`
--

INSERT INTO `emprunt_ouvrage` (`id_emprunt`, `id_ouvrage`, `rendu`) VALUES
(1, 1, '2020-02-11 17:27:01'),
(1, 2, '2020-02-11 17:28:11'),
(1, 3, '2020-02-11 17:29:11'),
(2, 5, NULL),
(2, 6, NULL);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `emprunt_ouvrage_non_rendu`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `emprunt_ouvrage_non_rendu`;
CREATE TABLE IF NOT EXISTS `emprunt_ouvrage_non_rendu` (
`id_emprunt` int(11)
,`id_ouvrage` int(11)
,`rendu` datetime
);

-- --------------------------------------------------------

--
-- Structure de la table `ouvrage`
--

DROP TABLE IF EXISTS `ouvrage`;
CREATE TABLE IF NOT EXISTS `ouvrage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) NOT NULL,
  `date_parution` date DEFAULT NULL,
  `auteur` varchar(50) DEFAULT NULL,
  `edition` varchar(50) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `isbn_10` char(10) DEFAULT NULL,
  `isbn_13` char(14) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn_10` (`isbn_10`),
  UNIQUE KEY `isbn_13` (`isbn_13`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ouvrage`
--

INSERT INTO `ouvrage` (`id`, `titre`, `date_parution`, `auteur`, `edition`, `photo`, `isbn_10`, `isbn_13`) VALUES
(1, 'Concevez votre site web avec PHP et MySQL', '2017-11-02', 'Mathieu Nebra', 'Eyrolles', '31uSiQ+htHL._SX352_BO1,204,203,200_.jpg', '2212674759', '978-2212674750'),
(2, 'Réalisez votre site web avec HTML 5 et CSS 3', '2017-09-07', 'Mathieu Nebra', 'Eyrolles', '41mAlr++iiL._SX352_BO1,204,203,200_.jpg', '2212674767', '978-2212674767'),
(3, 'Découvrez le langage JavaScript', '2017-01-12', 'Sébastien de la Marck', 'Eyrolles', '31ODoKzzHtL._SX352_BO1,204,203,200_.jpg', '2212143990', '978-2212143997'),
(4, 'Administrez vos bases de données avec MySQL', '2014-07-15', 'Chantal Gribaumont', 'OpenClassrooms', '31+Vi82ZLCL._SX346_BO1,204,203,200_.jpg', NULL, '979-1090085671'),
(5, 'jQuery - Le framework JavaScript du Web 2.0', '2014-05-14', 'Luc Van Lancker', 'Eni', '51khMJk822L._SX409_BO1,204,203,200_.jpg', '2746089173', '978-2746089174'),
(6, 'Bootstrap 4 pour l\'intégrateur web', '2018-07-11', 'Christophe Aubry', 'Editions ENI', '51KpsmIA1lL._SX411_BO1,204,203,200_.jpg', '2409014410', '978-2409014413');

-- --------------------------------------------------------

--
-- Structure de la vue `emprunt_non_clos`
--
DROP TABLE IF EXISTS `emprunt_non_clos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `emprunt_non_clos`  AS  select `emprunt`.`id` AS `id`,`emprunt`.`debut` AS `debut`,`emprunt`.`cloture` AS `cloture`,`emprunt`.`id_adherent` AS `id_adherent` from `emprunt` where isnull(`emprunt`.`cloture`) ;

-- --------------------------------------------------------

--
-- Structure de la vue `emprunt_ouvrage_non_rendu`
--
DROP TABLE IF EXISTS `emprunt_ouvrage_non_rendu`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `emprunt_ouvrage_non_rendu`  AS  select `emprunt_ouvrage`.`id_emprunt` AS `id_emprunt`,`emprunt_ouvrage`.`id_ouvrage` AS `id_ouvrage`,`emprunt_ouvrage`.`rendu` AS `rendu` from `emprunt_ouvrage` where isnull(`emprunt_ouvrage`.`rendu`) ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `emprunt_ibfk_1` FOREIGN KEY (`id_adherent`) REFERENCES `adherent` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `emprunt_ouvrage`
--
ALTER TABLE `emprunt_ouvrage`
  ADD CONSTRAINT `emprunt_ouvrage_ibfk_1` FOREIGN KEY (`id_emprunt`) REFERENCES `emprunt` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `emprunt_ouvrage_ibfk_2` FOREIGN KEY (`id_ouvrage`) REFERENCES `ouvrage` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
